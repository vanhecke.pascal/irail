var svgstore = require("svgstore");
var fs = require("fs");

var sprites = svgstore()
    .add(
        "flag-checkered",
        fs.readFileSync("./public/assets/images/svgstore/flag-checkered.svg", "utf8")
    )
    .add(
        "circle-o",
        fs.readFileSync("./public/assets/images/svgstore/circle-o.svg", "utf8")
    )
    .add(
        "flag",
        fs.readFileSync("./public/assets/images/svgstore/flag.svg", "utf8")
    )
    .add(
        "search",
        fs.readFileSync("./public/assets/images/svgstore/search.svg", "utf8")
    )
 

fs.writeFileSync("./public/assets/images/sprites.svg", sprites);
