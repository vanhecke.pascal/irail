import { configure,addDecorator } from '@storybook/react';
import { ThemeProvider } from 'styled-components';
import React from 'react';

import Theme from './../src/theme';
import './../src/theme/global-styles.js';
const req = require.context('../src/components', true, /\.stories\.js$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

addDecorator((story) => (
  <ThemeProvider theme={Theme}>
    {story()}
  </ThemeProvider>
))

configure(loadStories, module);