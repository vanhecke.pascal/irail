const theme = {
  colors: {
    brandPrimary: '#006AB3',
    brandPrimaryLight: '#E5F0F3',
    brandPrimaryOpaque: 'rgba(0,106,179,0.9)',
    brandSecondary: '#00B2E9',
    brandTertiary: '#f34d0d',
    brandTertiaryGradient: 'linear-gradient(to right, #f3560d 0%, #ffbf00 100%)',
    grey: '#565656',
    greyDark: '#333',
    greyLight: '#A7A7A7',
    greyLighter: '#E5E5E5',
    greyBody: '#F7F7F4',
    white: '#FFF'
  },
  fonts: {
    fontface: '"Lato",sans-serif',
    fontfaceLight: '"latolight",sans-serif',
    fontfaceRegular: '"latoregular",sans-serif',
    fontfaceBold: '"latoblack",sans-serif',
    sizeH1: '160px',
    sizeH1SemiSmall: '100px',
    sizeH1Small: '80px',
    sizeH2: '50px',
    sizeH3: '20px',
    sizeBase: '14px',
    sizeSmall: '12px',
    sizeSuperSmall: '10px'
  },
  zIndex: {}
};

export default theme;
