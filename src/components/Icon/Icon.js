import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

//const IconWrapper = styled.svg`${props => props.fill && css``};`;

const IconWrapper = styled.svg``

class Icon extends React.Component {
  render() {
    const { id } = this.props;
    const svgPath = `/assets/images/sprites.svg#${id}`;
    return (
      <IconWrapper>
        <use xlinkHref={svgPath} />
      </IconWrapper>
    );
  }
}

Icon.propTypes = {
  id: PropTypes.string
};

export default Icon;
