import React from 'react';
import styled, { css } from 'styled-components';

import Input from '../Input/Input';
import Icon from '../Icon/Icon';

//const IconWrapper = styled.svg`${props => props.fill && css``};`;

const InputDestinationWrapper = styled.div`
    background: ${props => props.theme.colors.brandPrimary};
    padding:30px 30px 30px 30px;
    
    &:after{
        content:'';
        width:5px;
        height:25px;
        position:absolute;
        border-left:4px dotted #FFF;
        left: 42.5px;
            top: 68px;
        opacity:0.3;
    }
`

const InputContainer = styled.div`
    position:relative;    
`

const InputWrapper = styled.div`
    display:flex;
    align-items: center;
    
    svg{
        width:15px;
        height:12px;
        transform:translate(0,-5px);
        flex-shrink: 0
    }
    
    input{
        margin-left:15px;
        margin-bottom:10px;
        position:relative;
        z-index:100;
        
        &:focus{
            z-index:0;
        }
    }
`

const Button = styled.button`
    /*background:none;
    border:none;
    outline:none;
    padding: 10px;
    flex-shrink: 0;
    cursor:pointer;
    text-align:right;
    
    svg{
        width:20px;
        height:20px;
    }*/
    background:none;
    border:none;
    outline:none;
    position:absolute;
    right:0;
    top:0;
    width:40px;
    height:calc(100% - 10px);
    
    svg{
        height:20px;
        width:20px;
        position:absolute;
        left:50%;
        top:6px;
        transform:translate(-50%,0);
        
        &:last-of-type{
            top:auto;
            bottom:10px;
        }
    }
`

class InputDestination extends React.Component {
    render() {
        
        return (
            <InputDestinationWrapper>
                <InputContainer>
                    <InputWrapper>
                        <Icon id="circle-o"/>
                        <Input placeholder="Vertrekstation" />
                    </InputWrapper>
                    <InputWrapper>
                        <Icon id="flag"/>
                        <Input placeholder="Eindstation" />
                    </InputWrapper>
                    <Button type="submit">
                        <Icon id="search"/>
                        <Icon id="search"/>
                    </Button>
                </InputContainer>
            </InputDestinationWrapper>
        );
    }
}

export default InputDestination;
