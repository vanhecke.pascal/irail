import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Input from './Input';

storiesOf('Input', module)
  .add('default', () => <Input placeholder="E-mailadres" />)
  .add('default error', () => <Input placeholder="E-mailadres" error />)
  .add('search', () => <Input search />)
  .add('search error', () => <Input search error />)
  .add('large', () => <Input large placeholder="Large input" />);
