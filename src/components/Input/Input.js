import styled, { css } from 'styled-components';

const Input = styled.input`
  border: none;
  border-bottom: 1px solid ${props => props.theme.colors.brandPrimaryLight};
  background: ${props => props.theme.colors.brandPrimary};
  font-size: ${props => props.theme.fonts.sizeBase};
  padding: 10px 0;
  outline: none;
  font-family: ${props => props.theme.fonts.fontfaceLight};
  line-height: 1em;
  width: 100%;
  -webkit-appearance: none;
  border-radius: 0;
  color:${props => props.theme.colors.white};

  &[type='number'] {
    -moz-appearance: textfield;
  }
  &[type='number']:hover,
  &[type='number']:focus {
    -moz-appearance: number-input;
  }
  &[type='number']::-webkit-inner-spin-button,
  &[type='number']::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
  }

  &::-webkit-input-placeholder {
    /* WebKit, Blink, Edge */
    color: ${props => props.theme.colors.brandPrimaryLight};
    line-height: normal !important;
  }
  &:-moz-placeholder {
    /* Mozilla Firefox 4 to 18 */
    color: ${props => props.theme.colors.brandPrimaryLight};
    opacity: 1;
  }
  &::-moz-placeholder {
    /* Mozilla Firefox 19+ */
    color: ${props => props.theme.colors.brandPrimaryLight};
    opacity: 1;
  }
  &:-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    color: ${props => props.theme.colors.brandPrimaryLight};
  }
  &::-ms-input-placeholder {
    /* Microsoft Edge */
    color: ${props => props.theme.colors.brandPrimaryLight};
  }

  ${props => props.error && css`border-bottom-color: red;`}
`;
export default Input;
